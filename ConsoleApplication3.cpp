

#include <iostream>

void printOddNumbers(int limit, bool isOdd)
{
    int i;

    if (isOdd) 
    {
         i = 1;
    }
    else
    {
         i = 0;
    }
    
    for (; i <= limit; i += 2) 
    {
        std::cout << i << " ";
    }

    std::cout << "\n";
}

int main()
{
    const int n = 20;
    
    for (int i = 0; i <= n; i += 2) 
    {
        std::cout << i << " ";
    }

    std::cout << "\n";
    
    printOddNumbers(n, true);
}

